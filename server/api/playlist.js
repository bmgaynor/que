const {Router} = require('express')
const router = Router()

const { getUser,
    getUserPlaylists,
    createPlaylist,
    getThePlaylist,
    addTracksToPlaylist,
    getPlaylistTracks,
    removeTracksFromPlaylist } = require('../services/playlist')

router.get('/getUserPlaylists', (req, res, next) => {
  console.log('in playlists')
  return getUser({spotifyApi: req.spotifyApi})
    .then((user) => getUserPlaylists({spotifyApi: req.spotifyApi, user}))
    .then(playlists => res.json(playlists))
    .catch(err => res.status(500).send(err))
})

router.put('/createPlaylist/:name', (req, res, next) => {
  const name = req.params.name

  return getUser({spotifyApi: req.spotifyApi})
    .then(user => createPlaylist({spotifyApi: req.spotifyApi, playlistName: name, user}))
    .then(playlist => res.json(playlist))
    .catch(err => res.status(500).send(err))
})

router.post('/addToPlaylist/:track', (req, res, next) => {
  const tracks = [req.params.track] // todo: update to use body and send array
  const spotifyApi = req.spotifyApi

  return getThePlaylist({ spotifyApi })
    .then(({user, playlist}) => addTracksToPlaylist({spotifyApi, playlistId: playlist.id, user, tracks}))
    .then(playlist => res.json(playlist))
    .catch(err => res.status(500).send(err))
})

router.post('/removeFromPlaylist/:track', (req, res, next) => {
  const tracks = [req.params.track]
  const spotifyApi = req.spotifyApi

  return getThePlaylist({ spotifyApi })
    .then(({ user, playlist }) => removeTracksFromPlaylist({spotifyApi, playlistId: playlist.id, user, tracks}))
    .then(playlist => res.json(playlist))
    .catch(err => res.status(500).send(err))
})

router.get('/thePlaylistTrackList', (req, res, next) => {
  const spotifyApi = req.spotifyApi

  return getThePlaylist({spotifyApi})
    .then(({user, playlist}) => getPlaylistTracks({ spotifyApi, playlist, user }))
    .then((playlistInfo) => res.json(playlistInfo))
    .catch(err => res.status(500).send(err))
})

module.exports = router
