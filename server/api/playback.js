const {Router} = require('express')
const router = Router()

const skipToNext = ({spotifyApi}) => {
  return spotifyApi.skipToNext()
    .then(function (data) {
      return data.body
    }, function (err) {
      throw new Error('Something went wrong!', err)
    })
}

router.get('/skip', (req, res, next) => {
  return skipToNext({spotifyApi: req.spotifyApi})
    .then(() => res.send('done'))
    .catch(err => res.status(500).send(err))
})

module.exports = router
