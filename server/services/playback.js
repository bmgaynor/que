
const getMyCurrentPlaybackState = ({spotifyApi}) => {
  return spotifyApi.getMyCurrentPlaybackState()
    .then(function (data) {
      return data.body
    }, function (err) {
      throw new Error('Something went wrong!', err)
    })
}

module.exports = {
  getMyCurrentPlaybackState
}
