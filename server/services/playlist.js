const PLAYLISTNAME = 'Que'

const getUserPlaylists = ({spotifyApi, user}) => {
  return spotifyApi.getUserPlaylists(user.id)
      .then(function (data) {
        // console.log('Retrieved playlists', data.body)
        return data.body
      }, function (err) {
        throw new Error('Something went wrong!', err)
      })
}

const getUser = ({spotifyApi}) => {
  return spotifyApi.getMe()
      .then(function (data) {
        return data.body
      }, function (err) {
        throw new Error('Something went wrong!', err)
      })
}

const createPlaylist = ({spotifyApi, playlistName, user}) => {
  return spotifyApi.createPlaylist(user.id, playlistName, {'public': true})
      .then(function (data) {
        console.log('created playlist named', playlistName)
        return data.body
      }, function (err) {
        console.log(err)
        throw new Error('Something went wrong!', err)
      })
}

const createTHEPlaylist = ({ spotifyApi }) => {
  return getUser({spotifyApi})
        .then(user => createPlaylist({ spotifyApi, playlistName: PLAYLISTNAME, user }))
}

const addTracksToPlaylist = ({spotifyApi, playlistId, user, tracks}) => {
  console.log({spotifyApi, playlistId, user, tracks})
  return spotifyApi.addTracksToPlaylist(user.id, playlistId, tracks)
      .then(function (data) {
        console.log('Added Tracks', tracks, 'to playlist', playlistId)
        return data.body
      }, function (err) {
        console.log(err)
        throw new Error('Something went wrong!', err)
      })
}

const getThePlaylist = ({spotifyApi}) => {
  return getUser({spotifyApi})
      .then(user => getUserPlaylists({spotifyApi, user}))
      .then(playlists => {
        const playlist = playlists.items.find((playlist) => playlist.name === PLAYLISTNAME)

        if (!playlist || !playlist.owner) {
          throw new Error('failed to get THE playlist')
        }
        return {
          playlist: playlist,
          user: playlist.owner
        }
      })
}

const getPlaylistTracks = ({spotifyApi, playlist, user}) =>
    spotifyApi.getPlaylistTracks(user.id, playlist.id)
        .then(res => res.body.items.map(item => item.track))

const removeTracksFromPlaylist = ({ spotifyApi, playlistId, user, tracks }) =>
  spotifyApi.removeTracksFromPlaylist(user.id, playlistId, tracks)
    .then(function (data) {
      console.log('Remove Tracks', tracks, 'from playlist', playlistId)
      return data.body
    }, function (err) {
      console.log(err)
      throw new Error('Something went wrong!', err)
    })

module.exports = {
  getUserPlaylists,
  getUser,
  createPlaylist,
  addTracksToPlaylist,
  getThePlaylist,
  getPlaylistTracks,
  PLAYLISTNAME,
  createTHEPlaylist,
  removeTracksFromPlaylist
}
