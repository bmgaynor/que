const express = require('express')
const SpotifyWebApi = require('spotify-web-api-node')
const app = express()
const playlist = require('./api/playlist')
const user = require('./api/user')
const playback = require('./api/playback')
const Sessions = require('./Sessions')
const { getThePlaylist, createTHEPlaylist } = require('./services/playlist')

const queUpURI = process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : 'https://que-up.surge.sh'
const redirectUri = process.env.NODE_ENV === 'development' ? 'http://localhost:8080/authorized' : 'https://damp-hollows-67395.herokuapp.com/authorized'

const spotifyApi = new SpotifyWebApi({
  clientId: 'f374e19cfd4b41dcab8da902a4540dca',
  clientSecret: process.env.spotify_secret, // config var on heroku
  redirectUri
})

const sessions = new Sessions({spotifyApi})

const refreshAuth = () => {
  spotifyApi.refreshAccessToken()
    .then(function (data) {
      console.log('The access token has been refreshed!')

      // Save the access token so that it's used in future calls
      spotifyApi.setAccessToken(data.body['access_token'])
    }, function (err) {
      console.log('Could not refresh access token', err)
    })
}

// Retrieve an access token
spotifyApi.clientCredentialsGrant()
  .then(function (data) {
    console.log('The access token expires in ' + data.body['expires_in'])
    console.log('The access token is ' + data.body['access_token'])

    // Save the access token so that it's used in future calls
    spotifyApi.setAccessToken(data.body['access_token'])
  }, function (err) {
    console.log('Something went wrong when retrieving an access token', err.message)
  })

app.use((req, res, next) => {
  // todo: conditionally set the spotifyApi to the correct session
  req.spotifyApi = spotifyApi
  next()
})

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Credentials')
  next()
})

app.get('/', (req, res, next) => {
  res.send('spotify QUE backend')
})

app.get('/login', (req, res) => {
  const scopes = [
    'user-read-private',
    'playlist-modify-private',
    'playlist-modify-public',
    'streaming',
    'user-modify-playback-state'
  ]
  res.redirect(spotifyApi.createAuthorizeURL(scopes, 'somestate'))
})

app.get('/authorized', (req, res) => {
  const code = req.query.code
  sessions.createNewSession(code)

  return spotifyApi.authorizationCodeGrant(code)
    .then(data => {
      console.log('The token expires in ' + data.body['expires_in'])
      console.log('The access token is ' + data.body['access_token'])
      console.log('The refresh token is ' + data.body['refresh_token'])

      // Set the access token on the API object to use it in later calls
      spotifyApi.setAccessToken(data.body['access_token'])
      spotifyApi.setRefreshToken(data.body['refresh_token'])
      res.redirect(queUpURI)  // todo: switch back to the home page

      getThePlaylist({spotifyApi})
        .then(() => {
          console.log('THE playlist exists and works')
        })
        .catch(() => {
          console.log('no playlist found! creating playlist')
          return createTHEPlaylist({spotifyApi})
        })
    }, err => {
      console.log('Something went wrong!', err)
    })
})

app.get('/me', (req, res) => {
  return spotifyApi.getMe()
    .then(function (data) {
      res.json(data.body)
      console.log('Some information about the authenticated user', data.body)
    }, function (err) {
      res.json({})
      console.log('Something went wrong!', err)
    })
})

app.use('/playlist', playlist)
app.use('/user', user)
app.use('/playback', playback)

app.set('port', (process.env.PORT || 8080))

app.listen(app.get('port'), function () {
  console.log('Node app is running at localhost:' + app.get('port'))
})
